package com.pdisoftware.labs.test;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WidgetTest {
  private Widget widget;

  @Before
  public void init() {
    widget = new Widget();
  }

  @Test
  public void getId() {
    widget.setId(1);

    assertEquals(1, widget.getId());
  }
}